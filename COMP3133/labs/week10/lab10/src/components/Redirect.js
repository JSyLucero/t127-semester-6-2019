import React, { Component } from 'react';

const Redirect = (props) => {
  const handleRedirectClick = (e) => {
    const { history } = props;
    if (history) history.push("/");
    else console.log("history not found in props"); 
  }

  return (
    <button onClick={handleRedirectClick}>Redirect</button>
  )
}

export default Redirect;