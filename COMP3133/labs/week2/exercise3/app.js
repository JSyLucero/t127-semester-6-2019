const capitalize = ([first, ...letters]) => `${first.toUpperCase()}${letters.join('').toLowerCase()}`;

const colors = [ 'red', 'green', 'blue' ];

const capitalizedColors = colors.map(x => capitalize(x));

console.log(capitalizedColors);