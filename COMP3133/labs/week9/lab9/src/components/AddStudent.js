import React, { Component } from 'react';
import axios from "axios";

class AddStudent extends Component {
  state = {  
    name: null,
    getStudents: this.props.getStudents ? this.props.getStudents : null
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    if (prevState) {
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
    }
    
    return state;
  }

  handleChange = (event) => {
    this.setState({ name: event.target.value.trim() });
  }

  handleKeyPress = (event) => {
    if (event.key == 'Enter')
      this.handleSubmit(event);
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { name, getStudents } = this.state;
    
    if (name) {
      axios.post('http://jsonplaceholder.typicode.com/users', { user: { name: name }})
        .then((response) => {
          console.log(response.data);
          if (getStudents)
            getStudents()
        });
    }
  }

  render() { 
    return (  
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>
            Person Name:&nbsp;
            <input type="text" name="name" onChange={this.handleChange} onKeyPress={this.handleKeyPress}></input>
          </label>
          <button type="submit">Add</button>
        </form>
      </div>
    );
  }
}
 
export default AddStudent;