const process = require('process'),
os = require("os");

process.stdin.setEncoding('utf8');

process.stdin.on('readable', () => {
  let chunk;
  let data = "";

  // Use a loop to make sure we read all available data.
  while ((chunk = process.stdin.read()) !== null) {
    data += chunk.split(os.EOL)[0];
  }

  if (data !== "")
    console.log(`Output: ${data.toUpperCase()}`);
  
  process.stdout.write("Input: ");
});

//CTRL + C
process.on('SIGINT', () => {
  console.log("^C");
  process.exit(1);
})

//CTRL + Z
process.on("SIGSTOP" , () => {
  console.log("^X");
  process.exit(1);
})