const process = require('process');

const output = 
`==System==
platform: ${process.platform}
architecture: ${process.arch}

==NodeJS==
node version: ${process.version}
V8 version: ${process.versions.v8}
Libuv version: ${process.versions.uv}
==Process==
pid: ${process.pid}
title: ${process.title}
current directory: ${process.cwd()}`

process.stdout.write(output);