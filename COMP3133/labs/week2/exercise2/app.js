const capitalize = ([first, ...letters]) => `${first.toUpperCase()}${letters.join('').toLowerCase()}`;

console.log(capitalize("fooBar"));
console.log(capitalize("nodeJs"));