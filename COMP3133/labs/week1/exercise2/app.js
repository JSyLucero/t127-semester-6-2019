var max = (numArr) => {
  let max = 0;
  for (index in numArr)
    if (max < numArr[index])
      max = numArr[index];
  return max;
};

let nums_one = [1, 0, 1];
let nums_two = [0, -10, -20];
let nums_three = [1000, 510, 440];
console.log(max(nums_one));
console.log(max(nums_two));
console.log(max(nums_three));