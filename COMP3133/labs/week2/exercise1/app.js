/*
Convert to ES6 Code:
function greeter(myArray, counter) {
  var greetText = 'Hello';

  for (var index = 0; index < myArray.length; index++) {
    console.log(greetText + myArray[index]);
  }
}
greeter(['Randy Savage', 'Ric Flair', 'Hulk Hogan'], 3);

*/

var greeter = (myArray, counter) => {
  let greetText = 'Hello';

  for (item of myArray) {
    console.log(`${greetText} ${item}`);
  }
}
greeter(['Randy Savage', 'Ric Flair', 'Hulk Hogan'], 3);