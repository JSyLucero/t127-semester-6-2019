import React, { Component } from 'react';
import axios from 'axios';
import AddStudent from './AddStudent';
import DeleteAction from './DeleteAction';

class StudentList extends Component {
  state = {  
    users: []
  }

  componentDidMount() {
    this.getStudents();
  }

  getStudents = () => {
    this.setState({ users: [] });
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then((response) => {
        this.setState({ users: response.data }, () => {
          console.log(this.state.users);
        });
      });
  }

  render() {
    const { users } = this.state; 
    return (  
      <div>
        <AddStudent getStudents={this.getStudents}/>
        <ul>
          {users.map((e, i) => 
            <li key={`person-${e.id}`}>
              {e.name}<br/>
              <DeleteAction key={`delete-person-${e.id}`} id={e.id}/>
            </li>
          )}
        </ul>
      </div>
    );
  }
}
 
export default StudentList;