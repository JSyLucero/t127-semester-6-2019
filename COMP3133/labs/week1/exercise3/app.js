var right = (string) => {
  if (string.length >= 3)
    return `${string.substr(string.length - 3, string.length)}${string.substr(0, string.length - 3)}`;
  return string;
};

let str1 = "Python";
let str2 = "JavaScript";
let str3 = "Hi";

console.log(right(str1));
console.log(right(str2));
console.log(right(str3));