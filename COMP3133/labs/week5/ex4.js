const { fork } = require('child_process');

const child = fork('./compute.js');

child.send("start");

child.on("message", message => {
  console.log(`Listening on the parent ${message}`);
});