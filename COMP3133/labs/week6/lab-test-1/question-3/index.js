const delayedPromise = (num) => new Promise((resolve, reject) => {
  setTimeout(resolve, 500, num * num);
});

const delayedPromise2 = (num) => new Promise((resolve) => {
  setTimeout(resolve, 500, num * num);
});

const promises = [delayedPromise(6), delayedPromise2(7)];

Promise.all(promises)
  .then(result => console.log(result));