const EventEmitter = require('events');
const emitter = new EventEmitter();
const buf = Buffer.from([0x62, 0x75, 0x66, 0x66, 0x65, 0x72]);

emitter.on('convert', () => {
  buf.forEach(e => console.log(e));
  console.log("buffer converted");
});

emitter.emit('convert');