import React, { Component } from 'react';
import Courses from './courses';

const Student = (props) => {
  return (
    <p>
      Student <strong>{props.name}</strong> with student <strong>{props.number}</strong>
      <Courses enrolled={props.enrolled}/>
    </p>
  );
};

export default Student;