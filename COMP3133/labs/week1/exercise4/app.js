var angle_type = (angle) => {
  if (angle < 90)
    return "Acute Angle";
  if (angle == 90)
    return "Right Angle";
  if (angle < 180)
    return "Obtuse Angle";
  if (angle == 180)
    return "Straight Angle";
}

console.log(angle_type(47));
console.log(angle_type(90));
console.log(angle_type(145));
console.log(angle_type(180));