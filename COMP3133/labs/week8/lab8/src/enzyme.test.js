import React, { Component } from 'react';
import { shallow, mount, render } from 'enzyme';

const sum = (x, y) => {
  return (
    <h1>{x + y}</h1>
  );
};

describe("When testing with Enzyme", () => {
  it("renders a h1", () => {
    const wrapper = shallow(sum(1, 2));
    expect(wrapper.find("h1").length).toBe(1);
  });
});