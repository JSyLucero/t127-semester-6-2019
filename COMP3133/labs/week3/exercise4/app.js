const delayedMultiply = (num) => {
  setTimeout(() => {
    console.log('done!');
    return num * num;
  }, 500);
}

const delayedPromise = (num) => {
  return new Promise((resolve) => {
    setTimeout(resolve, 500, num*num);
  });
}

delayedPromise(5)
  .then(result => console.log(result));

const asyncPromise = async (num) => {
  return await new Promise((resolve) => {
    setTimeout(resolve, 500, num*num);
  });
}

asyncPromise(6)
  .then(result => console.log(result));