import React, { Component } from 'react';
import axios from "axios";

class DeleteStudent extends Component {
  state = {  
    id: this.props.id ? this.props.id : null
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let state = { };
    if (prevState) {
      for (let key in nextProps) {
        if (prevState.hasOwnProperty(key)) {
          if (nextProps[key] !== prevState[key])
            state[key] = nextProps[key];
        }
      }
    }
    
    return state;
  }

  handleSubmit = (event) => {
    const { id } = this.state;
    
    if (id) {
      console.log(`DELETE Person with ID ${id}`);
      axios.delete(`http://jsonplaceholder.typicode.com/users/${id}`)
        .then((response) => {
          console.log(response);
          console.log(response.data);
        });
    }
  }

  render() { 
    return (  
      <button type="submit" onClick={this.handleSubmit}>Delete</button>
    );
  }
}
 
export default DeleteStudent;