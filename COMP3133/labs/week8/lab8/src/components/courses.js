import React, { Component } from 'react';

const Courses = (props) => {
  let { enrolled } = props;
  if (typeof(enrolled) !== Array) {
    if (!isNaN(enrolled))
      enrolled = Number.parseInt(enrolled);
    enrolled = Array.from(Array(enrolled), (e, i) => e = e ? e : e = i);
  }
  return (
    enrolled.map((e, i) => 
      <React.Fragment>
        <p>Student is enrolled in <strong>{isNaN(e) ? e : `Course ${e}`}</strong></p>
      </React.Fragment> 
    )
  );
}

export default Courses;