import React, { Component } from 'react';
import logo from './logo.svg';
import StudentList from './components/StudentList';
import AddStudent from './components/AddStudent';
import DeleteStudent from './components/DeleteStudent';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <StudentList/>
      </div>
    );
  }
}

export default App;
