const multiplyNumbers = (arr) => {
  return new Promise((resolve, reject) => {
    let numbers = arr.filter(item => Number.isInteger(item));

    if (numbers.length > 0) {
      for (let index in numbers) {
        numbers[index] = numbers[index] * 5;
      }
      resolve(numbers);
    }
    
    else {
      reject("Values passed does not contain any numbers.");
    }
  });
}

const mixedArray = ['Matrix', 1, true, 2, false, 3];

multiplyNumbers(mixedArray)
  .then(result => console.log(result))
  .catch(error => console.log(error));