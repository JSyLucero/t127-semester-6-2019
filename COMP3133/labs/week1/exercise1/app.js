var string = "the quick brown fox";
console.log(string);

var count = 0;
for (let word of string.split(" ")) {
  if (count == 0 && string.length > 0)
    string = "";
  string = `${string.length > 0 ? `${string} ` : string}${word.charAt(0).toUpperCase()}${word.substr(1, word.length)}`; 
  count++;
}

console.log(string);