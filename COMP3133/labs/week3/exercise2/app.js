var makeUpperCase = (arr) => {
  return new Promise((resolve, reject) => {
    let temp = [];
    for (let item of arr) {
      if (typeof(item) === "string") {
        temp.push(item.toUpperCase());
      } else {
        resolve("Error: Not all items in the array are strings!");
        return;
      }
    }
    resolve(temp);
  });
}

var sortWords = (arr) => {
  return new Promise((resolve, reject) => {
    if (typeof(arr) === "array") {
      resolve(arr.sort());
    } else {
      reject(arr);
    }
  });
}

const arrayOfNames = ['jaxx', 'tiny', 'clay'];
const mixedArray = ['anarchy', 99, true];
makeUpperCase(arrayOfNames)
  .then(sortWords)
  .then((result) => console.log(result))
  .catch(error => console.log(error));

makeUpperCase(mixedArray)
  .then(sortWords)
  .then((result) => console.log(result))
  .catch(error => console.log(error));