const path = require('path');
const fs = require('fs');

let dir = process.cwd();
let ext = process.argv[2];
ext = 
  ext ?
    ext.startsWith(".") ?
      ext : `.${ext}`
  : `no extension given`;

console.log(`current working directory: ${dir}`);
console.log(`command arg - extensions: ${ext}`);

const files =  fs.readdir(dir, (err, files) => {
  if (err)
    throw err;
  
  if (files.length > 0) {
    const filteredFiles = files.filter(file => path.extname(file) == ext);
    if (filteredFiles.length > 0)
      filteredFiles.forEach(file => console.log(file));
    else
      console.log(`no files found with the ${ext} extension`);
  } else {
    console.log("no files in found in current directory");
  }
})