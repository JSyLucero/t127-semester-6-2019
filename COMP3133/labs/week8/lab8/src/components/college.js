import React, { Component } from 'react';

class College extends Component {
  render() {
    const { name, location } = this.props;
    return (
      <p>College <strong>{name}</strong> with {location}</p>
    );
  }
}

export default College;