import React, { Component } from 'react';
import history from './modules/History';
import { BrowserRouter, Router, Route, Switch, Link } from "react-router-dom";
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Error from './components/Error';
import Navigation from './components/Navigation';
import Student from './components/Student';
import Redirect from './components/Redirect';
import logo from './logo.svg';
import './App.css';

const NewRoute = () => {
  return (
    <div>
      <p>New Route</p>
    </div>
  );
}

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <div>
          <Navigation/>
          <Redirect history={history}/>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/newroute" component={NewRoute}/>
            <Route path="/about" component={About}/>
            <Route path="/contact" component={Contact}/>
            <Route path="/student/:studentname/:studentno?" component={Student}/>
            <Route path="*" component={Error}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
