# T127 Semester 6 2018 Course Labs
## Jullian Anthony Sy-Lucero | jullian.sy-lucero@georgebrown.ca
## Student ID: 100998164

### Courses
#### [COMP3097 - Mobile Application Development II](https://gitlab.com/JSyLucero/T127-Semester-6-2018/tree/master/COMP3097/labs)
#### [COMP3132 - Machine Learning with Python](https://gitlab.com/JSyLucero/T127-Semester-6-2018/tree/master/COMP3132/labs)
#### [COMP3133 - Full Stack Development II](https://gitlab.com/JSyLucero/T127-Semester-6-2018/tree/master/COMP3133/labs)