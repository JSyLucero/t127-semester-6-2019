const fs = require('fs');

const dir = process.cwd();
const logsDir = `${dir}/Logs`
if (fs.existsSync(logsDir)) {
  const files = fs.readdir(logsDir, (err, files) => {
    if (err) throw err;

    if (files.length > 0) {
      files.forEach(file => {
        fs.unlink(`${logsDir}/${file}`, (err) => {
          if (err) throw err;

          console.log(`deleting files...${file}`);
        });
      })
    } else {
      console.log(`no logs to be deleted in "${logsDir}, proceeding to delete directory..."`);
    }
    fs.rmdir(logsDir, (err) => {
      if (err) throw err;

      console.log("logs directory deleted");
    });
  });
} else {
  console.log("logs directory does not exist, no removal necessary");
}