# COMP3133 Assignment
### 100801047 - Jacky Phung
### 100998164 - Jullian Anthony Sy-Lucero
#### Heroku App Deployment: https://comp3133-fs-assignment.herokuapp.com/
#### Git Repository: https://github.com/jaepun/comp3133_assignment

## Initial Setup
1. Run `npm install` in your command line interface. That's it!

## Running the Server
1. Run `npm start` in your command line interface, its as simple as that.

## API Routes
#### Event Log: 
- `/api/events/` - Retrieves all event logs
#### Room History: 
- `/api/history`
  - GET Retrieves all room history
  - POST Retrieves specific room history when given room name