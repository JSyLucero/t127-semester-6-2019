const fs = require('fs');

const dir = process.cwd();
const logsDir = `${dir}/Logs`
if (!fs.existsSync(logsDir)) {
  fs.mkdirSync(logsDir);
}

process.chdir(logsDir);
const files = fs.readdir(process.cwd(), (err, files) => {
  if (err) throw err;

  if (files.length > 0) {
    createLog(process.cwd(), `log${files.length}.txt`);
  } else {
    for (let i = 0; i < 10; i++) {
      createLog(process.cwd(), `log${i}.txt`);
    }
  }

  function createLog(path, fileName) {
    fs.writeFile(`${path}/${fileName}`, `New Log File!`, (err) => {
      if (err) throw err;

      console.log(fileName);
    });
  }
})