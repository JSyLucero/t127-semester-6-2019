import React, { Component } from 'react';

const Student = ({ match }) => {
  const { studentname, studentno } = match.params;
  return (
    <div>
      <p>Student</p>
      <div>
        <div>
          The student name is "{studentname}"!
          { studentno &&
            <React.Fragment>
              <br/>The student no. is {studentno}
            </React.Fragment>
          }
        </div>
      </div>
    </div>
  )
}

export default Student;