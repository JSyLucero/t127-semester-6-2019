const buffer_array = Buffer.from([ 8, 6, 7, 5, 3, 0, 9 ]);
const buffer_string = Buffer.from("I'm a string!", "utf-8");

console.log(buffer_array);
console.log(buffer_string);

console.log(buffer_string.toString());
console.log(buffer_string.toString("hex"));
console.log(buffer_string.toString("utf8").substring(0, 10));

const joined_buffer = Buffer.from(buffer_array+buffer_string);

console.log(joined_buffer);
console.log(JSON.stringify(joined_buffer));