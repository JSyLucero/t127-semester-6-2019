# COMP3133 Full Stack Development II Course Labs
## Jullian Anthony Sy-Lucero | jullian.sy-lucero@georgebrown.ca
## Student ID: 100998164

### Labs
#### [Lab 1](https://github.com/georgebrowntech/comp-3133-labs-JSyLucero/week1)
#### [Lab 2](https://github.com/georgebrowntech/comp-3133-labs-JSyLucero/week2)
