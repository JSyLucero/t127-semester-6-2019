var array = [1, 2, 3, 4];

var calculateSum = array.reduce((sum, incr) => sum + incr);
var calculateProduct = array.reduce((prod, multi) => prod * multi);

console.log(calculateSum);
console.log(calculateProduct);