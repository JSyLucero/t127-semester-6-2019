/* First Completed Function Draft
var array_max_sum = (numArr, amount) => {
  let sum = 0;
  for (index in numArr) {
    let result = 0;
    for (let counter = 0; counter < amount; counter++) {
      if (Number.parseInt(index) + counter < numArr.length)
        result += numArr[Number.parseInt(index) + counter];
    }
    
    if (sum < result)
      sum = result;
  }
  return sum;
}
*/

/* Completed Function Optimization #1 */
var array_max_sum = (numArr, amount) => {
  let sum = 0;
  for (let index = 0; index < numArr.length; index++) {
    let kconsec = numArr.slice(index, index + amount);
    let result = kconsec.reduce((sum, incr) => (sum + incr)); 
    if (sum < result)
      sum = result;
  }
  return sum;
}

console.log(array_max_sum([1, 2, 3, 14, 5], 2));
console.log(array_max_sum([2, 3, 5, 1, 6], 3));
console.log(array_max_sum([9, 3, 5, 1, 7], 2));